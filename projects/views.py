from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import CreateProject

from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def project_list(request):
    list = Project.objects.filter(owner=request.user)

    context = {"list": list}
    return render(request, "projects/list.html", context)


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"show": project}

    return render(request, "projects/show.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            proj = form.save(False)
            proj.owner = request.user
            proj.save()
            return redirect("list_projects")
    else:
        form = CreateProject()

    context = {"create": form}
    return render(request, "projects/createproj.html", context)
