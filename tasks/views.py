from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required
import pandas as pd
from plotly.offline import plot
import plotly.express as px


# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = TaskForm()

    context = {"createtask": form}
    return render(request, "tasks/createtask.html", context)


@login_required
def task_list(request):
    tasks = Task.objects.filter(assignee=request.user)

    qs = Task.objects.all()
    task_data = [
        {
            "Task": x.name,
            "Start": x.start_date,
            "Due Date": x.due_date,
            "Assignee": x.assignee,
        }
        for x in qs
    ]
    # df = dataframe
    df = pd.DataFrame(task_data)

    fig = px.timeline(
        df,
        x_start="Start",
        x_end="Due Date",
        y="Task",
        color="Assignee",
    )

    fig.update_yaxes(autorange="reversed")
    gantt_plot = plot(fig, output_type="div")

    context = {"task": tasks, "plot_div": gantt_plot}
    return render(request, "tasks/tasklist.html", context)
