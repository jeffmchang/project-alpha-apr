from django.db import models
from projects.models import Project
from django.contrib.auth.models import User

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(
        help_text="YYYY-MM-DD HH:MM",
        null=True,
        blank=True,
    )

    week_number = models.CharField(max_length=2, blank=True)

    due_date = models.DateTimeField(
        help_text="YYYY-MM-DD HH:MM",
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE, null=True
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.week_number == "":
            self.week_number = self.start_date.isocalendar()[1]
        super().save(*args, **kwargs)
